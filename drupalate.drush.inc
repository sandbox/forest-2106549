<?php

/**
 * @file
 *  Drush command file for Drupalate module
 *   
 */

/**
 * Implementation of hook_drush_command().
 *
 * @return
 *   An array of information about this drupalate drush command
 *
 */
function drupalate_drush_command() {
  $items = array();

  // The 'drupalate' command
  $items['drupalate'] = array(
    'description' => "Initial upgrade of a Drupal 7 module to Drupal 8",
    'arguments'   => array(
      'module' => 'Enter the machine name of the module if you want to run through drupalate',
    ),
    'options'     => array(
      'spreads' => array(
        'description'   => 'Comma delimited list of modules.',
        'example-value' => 'pants, pathauto, token',
      ),
    ),
    'examples'    => array(
      'drush drupalate pants',
    ),
    'aliases'     => array('d8'),
    'bootstrap'   => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );

  $items['drupalate-usage'] = array(
    'description'        => 'Instructions on how to upgrade a module',
    'hidden'             => TRUE,
    'topic'              => TRUE,
    'bootstrap'          => DRUSH_BOOTSTRAP_DRUSH,
    'callback'           => 'drush_print_file',
    'callback arguments' => array(dirname(__FILE__) . '/drupalate-topic.txt'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'. This hook is optional. If a command
 * does not implement this hook, the command's description is used instead.
 *
 * This hook is also used to look up help metadata, such as help
 * category title and summary.  See the comments below for a description.
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string of help text for drush drupalate command .
 */
function drupalate_drush_help($section) {
  switch ($section) {
    case 'drush:drupalate':
      return dt("Help section for Drupalate module");

    case 'meta:drupalate:title':
      return dt("Currently, Drupalate has only a single command.");

    case 'meta:drupalate:summary':
      return dt("Drush extension to convert Drupal 7 modules to Drupal 8.");
  }
}

/**
 * Implementation of drush_hook_COMMAND_validate().
 * Mainly for Windows compatability bc we have nothing but time to include that. 
 *
 */
function drush_drupalate_validate() {
  if (drush_is_windows()) {
    // $name = getenv('USERNAME');
    // TODO: implement check for elevated process using w32api
    // as sudo is not available for Windows
    // http://php.net/manual/en/book.w32api.php
    // http://social.msdn.microsoft.com/Forums/en/clr/thread/0957c58c-b30b-4972-a319-015df11b427d
  }
  else {
    # check to see if passed module is installed
  }
}

/**
 * The drush drupalate command implementaion.
 *
 * @see drush_invoke()
 * @see drush.api.php
 */
function drush_drupalate($module_name = 'default') {

// @FIXME Interactivity commented out for development. 
//  drush_print("Ok, before we begin, let's make sure you have *everything* that you need: ");
//  drush_print(" 1. The name of your module to be created anew or converted from Drupal 7");

// @FIXME Egad we make them move it to a different directory instead of finding it?! O_o
//  drush_print(" 2. If converting, the Drupal 7 version of the module to convert from installed on this Drupal instance. (Doesn't have to be enabled, but needs to be in sites/all/modules, not a sub-directory such as \"Custom\")");

//  drush_print(" 3. The UNIX root path to where you want the converted module to be saved to. This will usually be the modules directory on a Drupal 8 installation. ");
//  drush_print(" ");
//  if (drush_prompt("Do you have  / know the information above and want to continue? (y/n)") == 'y') {
  drush_print('- - - - - - - - -');

  // Set up necessary classes.
  $fh = new FileHandler();	
  $drupalate = new Drupalator();
  $moduleObject = drupalate_select_module($drupalate, $fh);

  // initializes object, updates info file, returns module (object).   
  $module = drupalate_init_drupalate_class(&$moduleObject);    
        
  // walk through the steps  
  drush_print("Creating the new module directory ");
  drush_print(" ");
  drupalate_drush_create_new_module_directory($module, $fh);   
    
  // walk through the steps  
  drush_print("Creating Drupal 8 module structure ");
  drush_print(" ");
  $scaffold = new Scaffolder();

  drush_print("Creating Route(s)");
  $new_route_file = $scaffold->createRoutingFile($module);
  drush_print(" ");
        
  // Hardcoding for now    
  $new_file_path = $module->getNewModuleDirectory() . '/' . $module->getModuleName() . '.routing.yml';
  // @TODO - This could probably be passed in a single array of files to be created. 
  drupalate_create_file($module, $fh, $new_route_file, $new_file_path);

  drush_print("Building Controller(s)");
  $new_controller = $scaffold->buildControllers($module);
  //$new_controller = $scaffold->buildController($module->getModuleName());
  drush_print(" ");    

  // Hardcoding for now      
  $new_file_name = ucfirst($module->getModuleName()) . 'Controller.php';
  $new_file_path = $module->getNewModuleDirectory() . '/lib/Drupal/' . $module->getModuleName() . '/Controller/' . $new_file_name;
  // @TODO - This could probably be passed in a single array of files to be created. 
  // @FIXME: Need path to correct Controller directory.
  drupalate_create_file($module, $fh, $new_controller, $new_file_path);
 
  drush_print(" ... Mapped new module directory structure.");  
  drush_print(" ... Files mapped.");  
        
  if ($module->getOp() == 'create') {

    drush_print("Creating the module's info file ");
    drush_print(" ");
    drupalate_create_info_file($module, $fh);
    //drupalate_create_file($module, $fh, $new_route_file, $new_file_name);
    drush_print(" ");

    drush_print('--> [create] Building new module file ');
    $new_module_directory = drupalate_create_module_file($module, $fh);  
    } 
    
  elseif ($module->getOp() == 'upgrade') {
    drush_print('[upgrade] Upgrading Drupal 7 module to Drupal 8');
    drupalate_copy_module_to_new_module_directory($module, $fh);
      
    drush_print("Process main .module file");
    drush_print(" ");
    
    var_dump($module);
    $module_file = drupalate_convert_module_file($module, $fh);
    
    drush_print('Module directory is ' . $module->getNewModuleDirectory());
    drush_print('Module should be ' . $module->getNewModuleDirectory() . '/' . $module->getModuleName() . '.module');
    $check_file_data = $module->getFileData();
    var_dump($check_file_data);
    drush_prompt('about to tokenize the module file');
    drupalate_process_module_file($module, $fh);

    $module->convertToDrupal8();
    
    drush_print("Process main .install file");
    drush_print(" ");
    drupalate_process_install_file($module, &$fh);
    
    drush_print("Process all .php and include files");
    drush_print(" ");
    drupalate_process_module_functions($module, $fh);

    drush_print("Does the modules have TPLs? If so hand off to Twigifier to process.");
    drush_print(" ");
    drupalate_scan_for_tpls() ? twigifier_process_tpls($twig, $fh) : print("No TPLs found in this module.");

    drush_print("Does the module have any views? If so, do something with them");
    drush_print(" ");
      
    } 
      
    else {
      // @FIXME: Replace with proper error handling. 
      drush_print('Something unexpected happened'); 
      }      

    // print exit message, we're done. 
    drush_print("Seems to have gone reasonably well. Check output for list of missing mappings");
    drush_print(" ");

// }
// else {
//   drush_print("When you have the info, comeback and try again!");
// }

// finish up
  drush_print('- - - - - - - - -');
}

function drupalate_drush_create_new_module_directory($module, $fh) {
  if (!$fh->doesExist($module->getNewModuleDirectory()))
    {

    drush_print('First, we need to create a new module folder for "' . $module->getModuleName() . '",');
//   if (drush_prompt('would you like some information to help you select the parent directory? (y/n)') == 'y') {
//     drush_print(" ");
//     drush_print("---> In Drupal 8, all contrib modules go into this directory:");
//     drush_print("    '<site_root>/modules' folder.");
//     drush_print(" ");
//     drush_print("---> Your Current Working Directory (cwd) is: ");
//     drush_print("     " . getcwd());
//     drush_print(" ");
//     drush_print("---> With the information above and using an absolute path,");
//   }

    $module->setNewModuleDirectory(drush_prompt('Enter the parent directory where your new module will live') . '/' .  $module->getModuleName());  

    drush_print(" ");
    
    print("Current (D7) module directory is " . $module->getOldModuleDirectory());
    drush_print(" ");

    print("The new (D8) module directory is " . $module->getNewModuleDirectory());
    drush_print("  ");
    
    drupalate_create_new_module_directory($module, $fh);
  }
}

/**
 * Command argument complete callback. 
 * Provides argument values for shell completion.
 *
 * @return
 *  Array of popular fillings.
 */
function drupalate_module_complete() {
  return array('values' => array('default', 'zen'));
}
