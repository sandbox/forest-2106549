<?php
/** 
 * @file 
 *   Provides internal logic for module upgrading.    
 */
 
// @FIXME names
require_once 'drupalate.php.inc';
require_once 'Scaffolder.php';

/** 
 *  Class provides conversion specific functionality for converting Drupal 7 modules into Drupal 8 modules
 *
 */
#class Drupalator {
class Drupalator {

  private $module;
  public $moduleName;
  private $moduleInfo;
  private  $moduleData;
  private $moduleDescription;
  private $oldModuleDirectory;
  private $newModuleDirectory;
  private $newDirectoryStructure;
  private $fileData;
  private $function;

 /**
  * Member function to set module name and directory.
  * @moduleData obj containing module name & filepath.
  */
  public function init(&$moduleData) {

    $this->setmoduleData($moduleData);
    $this->setModuleName($moduleData->name);
    $this->setOldModuleDirectory($moduleData->filepath);

  }

  /**
   *
   * @param $upgrader_class
   *
   */
  function createNewFile($new_file) {
    $new_file_string = $this->buildFile($new_file);    	
        
    return $new_file_string;
  }

  /**
   * Returns an array of elements needed to create a file. 
   * Should accept and array, not a string.
   */
  public function createFile($module_info) {
    $module_info = $this->getModuleInfo();
    if (isset($module_info)) {
      // copy the array
      $new_file = $module_info;
      // make changes (maybe do some validation here to match name?
      return $new_info_file;
    }
  }

  /**
   * Returns string from array representing file data
   * 
   */
  function buildFile($array, $prefix = FALSE) {
    $new_file_string = implode("\n", $array);
    
    return $new_file_string;
  }


  /**
   *
   * @param $upgrader_class
   * Handler for .info file
   *
   */
  function assembleNewInfoFile() {
    $new_info_file    = $this->createInfoFile();
    $info_file_string = $this->buildInfoFile($new_info_file);
    //$info_file_string = $this->processInfoFile($info_file_string);
        
    return $info_file_string;
  }

  /**
   * Returns an array of elements needed to create an info file. 
   * @TODO: This should be moved into the Scaffolder class. 
   * 
   */
  public function createInfoFile() {
    $module_info = $this->getModuleInfo();
    if (isset($module_info)) {
    
      // copy the array
      $new_info_file = $module_info;
      // make changes (maybe do some validation here to match name?

      $new_info_file['name']     	= ucfirst($this->getModuleName());
      $new_info_file['type']     	= 'module';
      $new_info_file['description'] .= ' (New Drupal 8 Module created with the help of Drupalate.)';
      $new_info_file['core']       	= '8.x';
      // @TODO: Version should be removed in D7, still true for D8?
      $new_info_file['version']    	= '8.x';
      $new_info_file = array('name' => $new_info_file['name'], 'type' => $new_info_file['type'], 'description' => $new_info_file['description']) + $new_info_file;

      return $new_info_file;
    }
  }

  /**
   * Takes the fileData (which was parsed from the orig .info file)
   * and converts it into the format of the new info files
   */
  function buildInfoFile($array, $prefix = FALSE) {
    $info = '';

    unset($array['datestamp']);
    unset($array['project']);

    foreach ($array as $key => $value) {
      if (!is_array($value)) {      
        $info .= $key;
        $info .= ": " . $value . "\n";
        } 
        else {      
        $info .= $key . ":\n"; //$this->buildInfoFile($value, (!$prefix ? $key : "{$prefix}[{$key}]"));
        switch ($key) {  
          case 'dependencies':
            foreach ($value as $ckey => $cvalue) {
              $info .= "   - " . $cvalue . "\n";
            }
            break;  
          case 'files':
            foreach ($value as $ckey => $cvalue) {
              $info .= "   - " . $cvalue . "\n";
            }
            break;  
          case 'scripts':
            foreach ($value as $ckey => $cvalue) {
              $info .= "   - " . $cvalue . "\n";
            }
            break;       
          case 'stylesheets':
            foreach ($value as $vkey => $vvalue ) {
              $info .= "  " . $vkey . ":\n";
              foreach ($vvalue as $cvalue) {
                $info .= "   - " . $cvalue . "\n";
              }
            }
            break;
          }
        
        // Future support for prefixing. 
        //$info .= $this->buildInfoFile($value, (!$prefix ? $key : "{$prefix}[{$key}]"));   
      
      }
    }
    
    return $info;
  }

  /**
   * Any addditional processing of .info file
   */
  function processInfoFile($array, $prefix = FALSE) {
  
  }



  /**
   *
   * @param $upgrader_class
   *
   * @TODO this doesn't need to be its own member function, there should be only one class method for file assembly. 
   */
  function assembleNewModuleFile($name) {
  	$upperName = $name;
    $scaffold = new Scaffolder();
    $new_module_file    = $scaffold->createModuleFile();
    $module_file_string = $new_module_file;
    // $module_file_string = $this->buildModuleFile($new_module_file);
    //$info_file_string = $this->processInfoFile($info_file_string);
        
    return $module_file_string;
  }

  public function updateModuleInfo($mod_info) {
    
    $this->setModuleInfo(drupal_parse_info_file($this->getOldModuleDirectory() . '/' . $mod_info . '.info'));
  
  }
  
  /**
  * @scaffoldNewModule pass in a new module name to return an array of module data. 
  */
  public function scaffoldNewModule($mod_info) {
  
      
}







  /**
   * @function this is where the magic happens.
   * Takes contents of module files and converts to Drupal 8.
   */
  public function convertToDrupal8() {
    // read in some helper functions we need; and grrrrrrr: PHP functions always have global scope
    // @TODO?  make the "public" methods static methods of some class (and hide the rest)

// debugging
drush_print('drupalator.223: the data contents of the the file to be converted:');
$filedata = $this->getFileData();
var_dump($filedata);

    // get the upgraded version of file contents
    $result = php_string_to_drupal8_string($this->getFileData());

    // save upgraded file contents as original file. 
    $this->setFileData($result);
  }



  /**
   * Helper function the segregates the full filepaths of assets into sibling arrays 
   * with the extension as the array identifier so paths are preserved.
   * 
   * @FIXME: Needs better logic to handle sub-directories.
   * @TODO: Move into Upgrader class
   *
   */
  public function processModuleFiles() {

    if (isset($this->newDirectoryStructure)) {
      foreach ($this->newDirectoryStructure as $keys => $values) {
        switch ($keys) {
          case 'install':
            print("Found install file"); 
            break;
          case 'php':
            print("Found the following .php files:");
            
            foreach ($values as $key => $value) {
              print($value . PHP_EOL);
            };
            break;
          case 'inc': 
            print("Found the following .inc files:");
              foreach ($values as $key => $value) {
              print($value . PHP_EOL);  
            };
            break;
        }
      }
    }
  }









  //  Getters and Setters

  public function setmoduleData($moduleData) {
    $this->moduleData = $moduleData;
  }

  public function getmoduleData() {
    return $this->moduleData;
  }

  public function setModuleInfo($moduleInfo) {
    $this->moduleInfo = $moduleInfo;
  }

  public function getModuleInfo() {
    return $this->moduleInfo;
  }

  public function setModuleName($moduleName) {
    $this->moduleName = $moduleName;
  }

  public function getModuleName() {
    return $this->moduleName;
  }
  
  public function setModuleDescription($moduleDescription) {
    $this->moduleDescription = $moduleDescription;
  }

  public function getModuleDescription() {
    return $this->moduleDescription;
  }

  public function setOldModuleDirectory($oldModuleDirectory) {
    $this->oldModuleDirectory = $oldModuleDirectory;
  }

  public function getOldModuleDirectory() {
    return $this->oldModuleDirectory;
  }
  
  public function setNewModuleDirectory($newModuleDirectory) {
    $this->newModuleDirectory = $newModuleDirectory;
  }

  public function getNewModuleDirectory() {
    return $this->newModuleDirectory;
  }

  public function setNewDirectoryStructure($newDirectoryStructure) {
    $this->newDirectoryStructure = $newDirectoryStructure;
  }

  public function getNewDirectoryStructure() {
    return $this->newDirectoryStructure;
  }

  public function setNewDirectoryList($newDirectoryList) {
    $this->newModuleDirectory = $newDirectoryList;
  }

  public function getNewDirectoryList() {
    return $this->newDirectoryList;
  }


  public function setFileData($fileData) {
    $this->fileData = $fileData;
  }

  public function getFileData() {
    return $this->fileData;
  }
  
  public function getOp() {
    return $this->moduleData->op;
  }
  
  
  
}
