<?php
/** 
 * @file 
 *   Provides internal logic for module upgrading.    
 */
 
// @FIXME names
require_once 'drupalate.php.inc';
require_once 'Scaffolder.php';

/** 
 *  Class provides conversion specific functionality for converting Drupal 7 modules into Drupal 8 modules
 *
 */
#class Drupalator {
class Upgrader {

  private $module;
  public $moduleName;
  private $moduleInfo;
  private  $moduleData;
  private $moduleDescription;
  private $oldModuleDirectory;
  private $newModuleDirectory;
  private $newDirectoryStructure;
  private $fileData;
  private $function;


  /**
   * @function this is where the magic happens.
   * Takes contents of module files and converts to Drupal 8.
   */
  public function convertToDrupal8() {
    // read in some helper functions we need; and grrrrrrr: PHP functions always have global scope
    // @TODO?  make the "public" methods static methods of some class (and hide the rest)

    // get the upgraded version of file contents
    $result = php_string_to_drupal8_string($this->getFileData());

    // save upgraded file contents as original file. 
    $this->setFileData($result);
  }



  /**
   * Helper function the segregates the full filepaths of assets into sibling arrays 
   * with the extension as the array identifier so paths are preserved.
   * 
   * @FIXME: Needs better logic to handle sub-directories.
   * @TODO: Move into Upgrader class
   *
   */
  public function processModuleFiles() {

    if (isset($this->newDirectoryStructure)) {
      foreach ($this->newDirectoryStructure as $keys => $values) {
        switch ($keys) {
          case 'install':
            print("Found install file"); 
            break;
          case 'php':
            print("Found the following .php files:");
            
            foreach ($values as $key => $value) {
              print($value . PHP_EOL);
            };
            break;
          case 'inc': 
            print("Found the following .inc files:");
              foreach ($values as $key => $value) {
              print($value . PHP_EOL);  
            };
            break;
        }
      }
    }
  }




  //  Getters and Setters

  public function setmoduleData($moduleData) {
    $this->moduleData = $moduleData;
  }

  public function getmoduleData() {
    return $this->moduleData;
  }

  public function setModuleInfo($moduleInfo) {
    $this->moduleInfo = $moduleInfo;
  }

  public function getModuleInfo() {
    return $this->moduleInfo;
  }

  public function setModuleName($moduleName) {
    $this->moduleName = $moduleName;
  }

  public function getModuleName() {
    return $this->moduleName;
  }
  
  public function setModuleDescription($moduleDescription) {
    $this->moduleDescription = $moduleDescription;
  }

  public function getModuleDescription() {
    return $this->moduleDescription;
  }

  public function setOldModuleDirectory($oldModuleDirectory) {
    $this->oldModuleDirectory = $oldModuleDirectory;
  }

  public function getOldModuleDirectory() {
    return $this->oldModuleDirectory;
  }
  
  public function setNewModuleDirectory($newModuleDirectory) {
    $this->newModuleDirectory = $newModuleDirectory;
  }

  public function getNewModuleDirectory() {
    return $this->newModuleDirectory;
  }

  public function setNewDirectoryStructure($newDirectoryStructure) {
    $this->newDirectoryStructure = $newDirectoryStructure;
  }

  public function getNewDirectoryStructure() {
    return $this->newDirectoryStructure;
  }

  public function setNewDirectoryList($newDirectoryList) {
    $this->newModuleDirectory = $newDirectoryList;
  }

  public function getNewDirectoryList() {
    return $this->newDirectoryList;
  }


  public function setFileData($fileData) {
    $this->fileData = $fileData;
  }

  public function getFileData() {
    return $this->fileData;
  }
  
  public function getOp() {
    return $this->moduleData->op;
  }
  
  
  
}
