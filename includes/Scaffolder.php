<?php
/** 
 * @file 
 *   Provides structure for new Drupal 8 modules.    
 */
 
 /** 
 *  Class provides specific functionality for creating Drupal 8 modules.
 *
 */
class Scaffolder {

  private $module;
  private $routing;
  private $controller;
  private $config;  


  /**
   * Returns string from array representing file data
   * 
   */
  function buildFile($array, $prefix = FALSE) {
    $new_file_string = implode("\n", $array);
    
    return $new_file_string;
  }


    /**
   * Returns array representing a new .module file. 
   * 
   */
function createModuleFile($name) {
  $nameUpper = $name;

  $new_module_file = array();   
  $new_module_file[] = '/**';
  $new_module_file[] = '* Implements hook_menu()';
  $new_module_file[] = '*/';
  $new_module_file[] = "function " .$name."_menu() {";
  $new_module_file[] = '$items[\'' . $name . "'] = array( ";
  $new_module_file[] = "  'title' => '".ucfirst($nameUpper).'\',';
  $new_module_file[] = "  'route_name' => '" . $name . "',";
  $new_module_file[] = '  );'; 

// @TODO: Only if user intereactively chooses to have a configuration page
// "  $items['" . $name . "config'] = array(" . PHP_EOL .
// "    'title' => '" . $nameUpper . " Configuration',
// "'route_name' => '" . $name . ".settings_form',
// "  );" . PHP_EOL .   
  $new_module_file[] = '  return $items;';
  $new_module_file[] = '}';
  
    return $new_module_file;
}

    /**
   * Returns array representing a new routing file. 
   * 
   */
  function createRoutingFile(&$module) {
    
	$name = $module->moduleName;
	$nameUpper = $name;
    //$set_sub_dir = $module->setSubDir('$subdir'); 
    
    // FOR PATH IN HOOK MENU
    // ADD D8 ROUTE TO ROUTING ARRAY
    $routing_file = array();
    $routing_file[] = $name . ':'; 
    $routing_file[] =     "  path: '/" . $name . "'";
    $routing_file[] =    '  defaults:';  
    // ugh, extra spaces. 
    $routing_file[] =  '    _content: \Drupal\ '.$name.'\Controller\ '.$nameUpper.'Controller::content';

  // @TODO - optionally create module settings_form if users selects. 
    
    return $routing_file;
  }

    /**
   * Returns an array representing a new controller.
   * FOR PATH IN HOOK MENU
   * ADD D8 CONTROLLER FOR ROUTE
   * 
   */
  function buildControllers($module) {
	$name = $module->moduleName;
    $nameUpper = $name;
    $subdir = 'lib/Drupal/' . $module->moduleName . '/Controller';

    $controller_file = array();
    $controller_file[] = "namespace Drupal\$name\Controller;";
    $controller_file[] = '    use Drupal\Core\Controller\ControllerBase;';
    $controller_file[] = 'class " . $upperName . "Controller extends ControllerBase {';
    $controller_file[] = '  public function content() {';
    $controller_file[] = '    $config = $this->config'."('$name.settings');";
    $controller_file[] = '    $case = $config->get(\'case\');';
    $controller_file[] = '    if ($case == \'upper\') {'; 
    $controller_file[] = '      $markup = $this'."->t('$upperName Module');";
    $controller_file[] = '    }';
    $controller_file[] = '    elseif ($case == \'title\') {';
    $controller_file[] = '      $markup = $this->t(\'' . "$upperName Module\');";
    $controller_file[] = '    }';
    $controller_file[] = '    $markup = t(\'$upperName Module\');';
    $controller_file[] = '    return array(';
    $controller_file[] = '      \'#markup\' => $markup,\'     );  }  }';

    return $controller_file;
  }
}


?>

  
  
  
  
